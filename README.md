# BackendDataDemo
作为服务器配置数据，为前端和移动端提供非逻辑处理数据。
主要用于需要后台修改小部分数据，且不用到处理逻辑的配置型数据。例如，移动端升级接口，配置信息接口等。
